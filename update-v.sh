#!/bin/bash

# Get the last commit date
CI_COMMIT_DATE=$(git log -1 --format=%cd --date=format:'%y%m%d')

# Get the first eight digits of the commit hash
CI_COMMIT_SHORT_SHA=$(git rev-parse --short=8 HEAD)
newVersion="${CI_COMMIT_DATE}-${CI_COMMIT_SHORT_SHA}"

# Print the new version
echo "Updating application version to: $newVersion"

# Pass the version as an argument to the Docker build (export make the var or fun avaliable for child)
echo "export VERSION=${newVersion}" >> $CI_PROJECT_DIR/version.env





