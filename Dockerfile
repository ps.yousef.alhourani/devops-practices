# Use a small, official, updated, and secure base image
FROM openjdk:11
ARG VERSION
# Create a non-root user
RUN adduser --system myuser

# Set the working directory
WORKDIR /app

# Copy your application artifact to the container
COPY target/assignment-0.0.1-SNAPSHOT.jar /app/app.jar

# Expose the port your application runs on (if applicable)
EXPOSE 8080

# Define environment variables for H2 profile and Java options
ENV SPRING_PROFILES_ACTIVE=default
ENV JAVA_OPTS="Djava.awt.headless=true"

# Run your application with the non-root user
USER myuser

# Command to start your application
CMD ["java", "-jar", "app.jar"]
